package models

import (
	"encoding/json"
	"io"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type date_interval uint8

const (
	year date_interval = iota
	month
	week
	day
)

type duration struct {
	Value    int           `bson:"value"`
	Interval date_interval `bson:"interval"`
}

type User struct {
	ID        primitive.ObjectID `bson:"_id"`
	Created   time.Time          `bson:"created"`
	Updated   time.Time          `bson:"updated"`
	FirstName string             `bson:"firstname"`
	LastName  string             `bson:"lastname"`
	Age       duration           `bson:"age"`
	MobileNo  string             `bson:"mobileno"`
	Active    bool               `bson:"active"`
}

func (user *User) FromJson(r io.Reader) error {
	e := json.NewDecoder(r)
	return e.Decode(user)
}

func (user *User) ToJson(r io.Writer) error {
	e := json.NewEncoder(r)
	return e.Encode(user)
}
