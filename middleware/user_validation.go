package middleware

import (
	"context"
	"fmt"
	"net/http"
	"regexp"
	"time"
	"user_crud/models"
)

type KeyUser struct{}

func ValidateUser(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		user := &models.User{}
		if r.Method == http.MethodPost {
			user.Created = time.Now()
		}
		user.Updated = time.Now()
		err := user.FromJson(r.Body)
		if err != nil {
			http.Error(rw, fmt.Sprintln("Unable to parse request. ", err), http.StatusBadRequest)
			return
		}
		phoneRegex, _ := regexp.Compile("[0-9]{10}")
		nameRegex, _ := regexp.Compile("[a-zA-Z]+")

		firstNameMatch := nameRegex.Match([]byte(user.FirstName))
		lastNameMatch := nameRegex.Match([]byte(user.LastName))
		phoneRegexMatch := phoneRegex.Match([]byte(user.MobileNo))

		if user.Age.Interval < 0 || user.Age.Interval > 3 || user.Age.Value < 1 || !firstNameMatch || !lastNameMatch || !phoneRegexMatch {
			http.Error(rw, "Invalid user data.", http.StatusBadRequest)
			return
		}

		ctx := context.WithValue(r.Context(), KeyUser{}, user)
		r = r.WithContext(ctx)
		next.ServeHTTP(rw, r)
	})
}
