package handlers

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"
	"user_crud/middleware"
	"user_crud/models"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const DATABASE_URI string = "mongodb://mongo1:27017"

var OPTIONS *options.ClientOptions = options.Client().ApplyURI(DATABASE_URI).SetDirect(true)

func UserGetAll(rw http.ResponseWriter, r *http.Request) {
	users, err := getDBEntries(bson.M{})
	if err != nil {
		http.Error(rw, fmt.Sprintln("Error while trying to fetch data from DB. ", err), http.StatusInternalServerError)
		return
	}
	data, err := json.Marshal(users)
	if err != nil {
		http.Error(rw, fmt.Sprintln("Unexpected error while trying to parse users. ", err), http.StatusInternalServerError)
		return
	}
	rw.Header().Add("Content-Type", "text/json")
	rw.WriteHeader(http.StatusOK)
	rw.Write(data)
}

func UserGet(rw http.ResponseWriter, r *http.Request) {
	passed_string := string(mux.Vars(r)["id"])
	id, err := primitive.ObjectIDFromHex(passed_string)
	if err != nil {
		rw.WriteHeader(http.StatusBadRequest)
		return
	}
	users, err := getDBEntries(bson.M{"_id": id, "active": true})
	if err != nil {
		http.Error(rw, fmt.Sprintln("Error while trying to fetch data from DB. ", http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	if len(users) != 1 {
		rw.WriteHeader(http.StatusNotFound)
		return
	}
	user := users[0]
	data, err := json.Marshal(user)
	if err != nil {
		http.Error(rw, fmt.Sprintln("Unexpected error while trying to parse user. ", err), http.StatusInternalServerError)
		return
	}
	rw.Header().Add("Content-Type", "text/json")
	rw.WriteHeader(http.StatusOK)
	rw.Write(data)
}

func UserCreate(rw http.ResponseWriter, r *http.Request) {
	user := r.Context().Value(middleware.KeyUser{}).(*models.User)
	user.ID = primitive.NewObjectID()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	client, err := mongo.Connect(ctx, OPTIONS)
	if err != nil {
		http.Error(rw, fmt.Sprintln("Could not connect to database. ", err), http.StatusInternalServerError)
		return
	}

	defer func() {
		err = client.Disconnect(ctx)
		if err != nil {
			http.Error(rw, fmt.Sprintln("Unexpected error while trying to close DB.", err), http.StatusInternalServerError)
			return
		}
	}()

	dbConn := client.Database("store")
	userCollection := dbConn.Collection("users")
	result, err := userCollection.InsertOne(ctx, user)
	rw.WriteHeader(http.StatusCreated)
	data, err := json.Marshal(result.InsertedID)
	rw.Write(data)
}

func UserModify(rw http.ResponseWriter, r *http.Request) {
	user := r.Context().Value(middleware.KeyUser{}).(*models.User)
	passed_string := string(mux.Vars(r)["id"])
	id, err := primitive.ObjectIDFromHex(passed_string)
	if err != nil {
		rw.WriteHeader(http.StatusBadRequest)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	client, err := mongo.Connect(ctx, OPTIONS)
	if err != nil {
		http.Error(rw, fmt.Sprintln("Could not connect to database. ", err), http.StatusInternalServerError)
		return
	}

	defer func() {
		err = client.Disconnect(ctx)
		if err != nil {
			http.Error(rw, fmt.Sprintln("Unexpected error while trying to close DB.", err), http.StatusInternalServerError)
			return
		}
	}()

	dbConn := client.Database("store")
	userCollection := dbConn.Collection("users")
	cursor, err := userCollection.Find(ctx, bson.M{"_id": id})
	if err != nil {
		http.Error(rw, fmt.Sprintln("Unexpected database error. ", err), http.StatusInternalServerError)
		return
	}
	var users []models.User
	err = cursor.All(ctx, &users)
	if err != nil {
		http.Error(rw, fmt.Sprintln("Unexpected database error. ", err), http.StatusInternalServerError)
		return
	}

	if len(users) != 1 {
		rw.WriteHeader(http.StatusNotFound)
		return
	}
	user.Created = users[0].Created
	result, err := updateDBEntries(bson.M{"_id": id}, bson.D{
		{"$set", user}})
	if err != nil {
		http.Error(rw, fmt.Sprintln("Unexpected error while trying to parse users. ", err), http.StatusInternalServerError)
		return
	}
	if result.ModifiedCount != 1 {
		rw.WriteHeader(http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
}

func UserDelete(rw http.ResponseWriter, r *http.Request) {
	passed_string := string(mux.Vars(r)["id"])
	id, err := primitive.ObjectIDFromHex(passed_string)
	if err != nil {
		rw.WriteHeader(http.StatusBadRequest)
		return
	}
	users, err := getDBEntries(bson.M{"_id": id, "active": true})
	if err != nil {
		http.Error(rw, fmt.Sprintln("Unexpected error while trying to parse users. ", err), http.StatusInternalServerError)
		return
	}
	if len(users) != 1 {
		rw.WriteHeader(http.StatusNotFound)
		return
	}
	result, err := updateDBEntries(bson.M{"_id": id}, bson.D{
		{"$set", bson.D{{"active", false}, {"updated", time.Now()}}}})
	if err != nil {
		http.Error(rw, fmt.Sprintln("Unexpected error while trying to parse users. ", err), http.StatusInternalServerError)
		return
	}
	if result.ModifiedCount != 1 {
		rw.WriteHeader(http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusNoContent)
}

func getDBEntries(filterStruct bson.M) ([]models.User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	client, err := mongo.Connect(ctx, OPTIONS)
	if err != nil {
		return nil, err
	}

	defer func() {
		err = client.Disconnect(ctx)
		if err != nil {
			fmt.Println("Unexpected error while trying to close database. ", err)
		}
	}()

	dbConn := client.Database("store")
	userCollection := dbConn.Collection("users")
	cursor, err := userCollection.Find(ctx, filterStruct)
	if err != nil {
		return nil, err
	}
	var users []models.User
	err = cursor.All(ctx, &users)
	if err != nil {
		return nil, err
	}
	return users, nil
}

func updateDBEntries(identityFields bson.M, modifiedFields bson.D) (*mongo.UpdateResult, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	client, err := mongo.Connect(ctx, OPTIONS)
	if err != nil {
		return nil, err
	}

	defer func() {
		err = client.Disconnect(ctx)
		if err != nil {
			fmt.Println("Unexpected error while trying to close Database. ", err)
		}
	}()
	dbConn := client.Database("store")
	userCollection := dbConn.Collection("users")
	result, err := userCollection.UpdateOne(ctx, identityFields, modifiedFields)
	if err != nil {
		return nil, err
	}
	return result, nil
}
