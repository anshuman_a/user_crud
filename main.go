package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"time"
	"user_crud/handlers"
	"user_crud/middleware"

	"github.com/gorilla/mux"
)

func main() {
	serverMux := mux.NewRouter()

	getRouter := serverMux.Methods(http.MethodGet).Subrouter()
	postRouter := serverMux.Methods(http.MethodPost).Subrouter()
	putRouter := serverMux.Methods(http.MethodPut).Subrouter()
	deleteRouter := serverMux.Methods(http.MethodDelete).Subrouter()

	getRouter.HandleFunc("/v1/users/{id:.+}", handlers.UserGet)
	getRouter.HandleFunc("/v1/users", handlers.UserGetAll)

	postRouter.HandleFunc("/v1/users", handlers.UserCreate)
	postRouter.Use(middleware.ValidateUser)

	putRouter.HandleFunc("/v1/users/{id:.+}", handlers.UserModify)
	putRouter.Use(middleware.ValidateUser)

	deleteRouter.HandleFunc("/v1/users/{id:.+}", handlers.UserDelete)

	server := &http.Server{
		Addr:         "localhost:9090",
		Handler:      serverMux,
		IdleTimeout:  120 * time.Second,
		ReadTimeout:  1 * time.Second,
		WriteTimeout: 1 * time.Second,
	}
	go func() {
		err := server.ListenAndServe()
		if err != nil {
			panic(err)
		}
	}()

	sigChan := make(chan os.Signal)
	signal.Notify(sigChan, os.Interrupt)
	signal.Notify(sigChan, os.Kill)

	sig := <-sigChan
	fmt.Println("Received Terminate signal. Shutting down. ", sig)
	tc, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	server.Shutdown(tc)
}
