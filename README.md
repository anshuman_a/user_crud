# User CRUD application built in GOlang
## To run, simply clone the repo and open with VSCode's remote container extension.

### The available paths in the app are:

- localhost:9090/v1/users **GET**
- localhost:9090/v1/users **POST**
- localhost:9090/v1/users/{id} **GET**
- localhost:9090/v1/users/{id} **PUT**
- localhost:9090/v1/users/{id} **DELETE**